/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*****************************************************************************/

#ifndef DEF_H_
#define DEF_H_ 0

typedef unsigned char 	u8;
typedef unsigned short 	u16;
typedef unsigned 		u32;
typedef signed char		s8;
typedef short			s16;
typedef int				s32;

//Parancsok
//Mozgás
#define STOP 		0
#define UP_SLOW		0x01
#define DOWN_SLOW	0x02
#define UP_FAST		0x05
#define DOWN_FAST	0x06
//Ajtó mozgatás
#define OPEN_DOOR	0x08
#define CLOSE_DOOR	0

//Gombok kívül
//F0_DOWN -- lehetetlen
#define F0_UP_CL	0x20
#define F1_DOWN_CL	0x40
#define F1_UP_CL	0x80
#define F2_DOWN_CL	0x0100
#define F2_UP_CL	0x0200
#define F3_DOWN_CL	0x0400
#define F3_UP_CL	0x0800
#define F4_DOWN_CL	0x1000
//F4_UP -- lehetetlen

//Gombok belül
#define B_0_CL		0x010000
#define B_1_CL		0x020000
#define B_2_CL		0x040000
#define B_3_CL		0x080000
#define B_4_CL		0x100000
#define CLOSE_DOOR_CL	0x200000
#define OPEN_DOOR_CL	0x400000
#define STOP_CL		0x800000


//Státusz bitek
//Gombok kívül
#define F0_UP_SET	0x04
//#define F0_DOWN_SET	0x08	lehetetlen
#define F1_UP_SET	0x40
#define F1_DOWN_SET	0x80
#define F2_UP_SET	0x0400
#define F2_DOWN_SET	0x0800
#define F3_UP_SET	0x4000
#define F3_DOWN_SET	0x8000
//#define F4_UP_SET	0x040000	lehetetlen
#define F4_DOWN_SET	0x080000

//Gombok belül
#define B_0_SET		0x01000000
#define B_1_SET		0x02000000
#define B_2_SET		0x04000000
#define B_3_SET		0x08000000
#define B_4_SET		0x10000000
#define CLOSE_DOOR_SET	0x20000000
#define OPEN_DOOR_SET	0x40000000
#define STOP_SET	0x80000000

//Szenzorok
#define COLL_TOP	0x400000
#define F0_TOP		0x01
#define F0_BOTTOM	0x02
#define F1_TOP		0x10
#define F1_BOTTOM	0X20
#define F2_TOP		0x0100
#define F2_BOTTOM	0x0200
#define F3_TOP		0x1000
#define F3_BOTTOM	0x2000
#define F4_TOP		0x010000
#define F4_BOTTOM	0x020000
#define COLL_BOTTOM 0x800000

//Ajtó szenzorok
#define DOOR_CLOSED	0x100000
#define DOOR_OPEN	0x200000

enum liftStates {	STOPPED = 0,		//áll, és emeleten áll, tehát ki/be lehet szállni, ha átváltunk WAITING állapotba
					WAITING,			//áll, ki/beszállásra várva
					STARTING,			//lassú indulás
					STOPPING,			//lassú megállás
					SLOWTOFAST,			//gyorsítás lassú mozgásról gyorsra
					FASTTOSLOW,			//lassítás gyors mozgásról lassúra
					MOVING_SLOW,		//ha csak 1 emeletet kell fel/le menni, akkor nincs idő teljesen felgyorsítani
			 		MOVING,				//lift mozgásban, max sebességgel
	 				STOP_FORCED			//áll, de a vészgomb miatt
				};

enum directions	{
					DIR_UP = 1,		//mozgás közben az aktuális irány
					DIR_DOWN		//álló helyzet esetén az, hogy milyen irányból jött
				};

enum doorStates {	CLOSED = 0,		//ajtó zárva
					OPENING,		//nyitódik
					OPEN,			//nyitva
					CLOSING,		//csukódik
				};		//az ajtó szenzorok alapján állítódik

//Lift státusz:
	extern int callUp[5];		//ha van az adott emeleten felfelé hívás, akkor igaz
	extern int callDown[5];		//ha van az adott emeleten lefelé hívás, akkor igaz
	extern int toGo[5];			//ha az adott emelet az úticélok között van (belső gombok), akkor igaz
	extern int sensorTop[5];		//az adott emelet felső szenzora jelez-e
	extern int sensorBottom[5];	//az adott emelet alsó szenzora jelez-e
	extern int forcedStop;		//be van-e nyomva a stop gomb
	extern int openDoor;		//<> gomb be van-e nyomva
	extern int closeDoor;		//>< gomb be van-e nyomva
	extern int doorClosed;		//ajtó zárva szenzor
	extern int doorOpen;		//ajtó nyitva szenzor
//

extern int staBits;			//a státusz bitek másolata
extern enum liftStates state;
extern enum directions dir;
extern enum doorStates doorState;
extern int dest;
extern int level;
extern int nextCmd;

extern int savedState;

#endif




