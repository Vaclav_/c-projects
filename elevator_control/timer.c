/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Ez a modul: egyszerű stopper. A doorState.c használja.
*****************************************************************************/

#include <sys/time.h>	//gettimeofday() fvhez
#include <unistd.h>
#include "def.h"

static struct timeval t1, t2;	//t1: idő a timer indításakor, t2: jelenlegi idő

void timerStart(void);
u32 timerCheck(void);			//indítás óta eltelt idő us-ban

void timerStart(void) {
	gettimeofday(&t1, NULL);
}

u32 timerCheck(void) {
	gettimeofday(&t2, NULL);
	u32 t;
	t = (t2.tv_sec - t1.tv_sec) * 1000000.0;
	t += (t2.tv_usec - t1.tv_usec);
	return t;
}
