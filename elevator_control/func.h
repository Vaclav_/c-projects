/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*****************************************************************************/

#ifndef FUNC_H
#define FUNC_H 0

#include "def.h"

//process.c
//feldolgozza a státusz biteket könyebben használható formába
void processSta(void);

//dest.c
//beállítja a level változót, ami tárolja, hogy melyik szinten van a lift
void setLevel(void);
//beállítja a dest változót, ami a következő állomás szintjének száma, ha nincs hívás, akkor pedig 0
void setDest(void);

//liftState.c
int initPosition(void);
void moveToDest(void);

//doorState.c
void controlDoor(void);

//getCmd.c
//visszatérési érték: igaz, ha vége a gyorsításnak/lassításnak, vagyis a következő állapot jön
//int *cmd: a következő parancs (csak lift mozgatás)
int cmdStarting( int *cmd );
int cmdStopping( int *cmd );
int cmdToFast( int *cmd );
int cmdToSlow( int *cmd );
int cmdSlow( int *cmd );
int openDataFile(void);

//timer.c
void timerStart(void);
u32 timerCheck(void);			//indítás óta eltelt idő us-ban

#endif
