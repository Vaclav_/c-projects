package BOX;

sub new
  {
  my ($t,$c,$x,$y)=@_;
  my $s={};
  
  my $sh=$c->createRectangle($x,$y,$x+24,$y+50,-width=>3,-outline=>'brown',-fill=>black);
  my $dr=$c->createRectangle($x+2,$y+2,$x+22,$y+48,-outline=>'lightgray',-fill=>'lightgray');  
  
  $s->{c}=$c;
  $s->{x}=$x;
  $s->{y}=$y;
  $s->{dr}=$dr;
  $s->{sh}=$sh;
  $s->{t}=0;
  $s->{td}=0;
  $s->{op}=0;
  $s->{cl}=1;  

  return bless($s,$t);    
  }
  
sub open
  {
  my $s=shift;

  if($s->{td}<4) { $s->{td}++; return; }
  
  my $c=$s->{c};
  my $x=$s->{x};
  my $y=$s->{y};
  my $dr=$s->{dr};
  my @cd=$c->coords($dr);
  
  $cd[0]++;
  
  if($cd[0]<$x+20) { $c->coords($dr,$cd[0],$cd[1],$cd[2],$cd[3]); $s->{cl}=0; $s->{op}=0; }
  else		   { $s->{op}=2; $s->{cl}=0; }
  $s->{td}=0;  
  }

sub close
  {
  my $s=shift;
  
  if($s->{td}<4) { $s->{td}++; return; }
  
  my $c=$s->{c};
  my $x=$s->{x};
  my $y=$s->{y};
  my $dr=$s->{dr};
  my @cd=$c->coords($dr);
  
  $cd[0]--;
  
  if($cd[0]>$x+2) { $c->coords($dr,$cd[0],$cd[1],$cd[2],$cd[3]); $s->{cl}=0; $s->{op}=0; }
  else		  { $s->{cl}=1; $s->{op}=0; }
  $s->{td}=0;
  }
  
sub test  
  {
  my $s=shift;
  
  return $s->{op}+$s->{cl};
  }

sub move
  {
  my $s=shift;
  my $d=shift;
  my $t=shift;
  
  my $sh=$s->{sh};
  my $dr=$s->{dr};
  my $c=$s->{c};

  if($d==0) 
    { 
    $s->{t}=0; 
    return;

    }
  if($s->{t}<$t)
    {
    $s->{t}++;
    return;
    }

  my @csh=$c->coords($sh);
  my @dsh=$c->coords($dr);
  
  if($d==1)
    {
    $c->move($sh,0,-1);   
    $c->move($dr,0,-1);   
    $s->{t}=0;     
    return;
    }
  
  if($d==-1)
    {
    $c->move($sh,0,1);   
    $c->move($dr,0,1);   
    $s->{t}=0;     
    return;
    }
  }

sub coords
  {
  my $s=shift; 
  my @p=$s->{c}->coords($s->{sh});
  
  return @p;
  }

package TBL;

sub new
  {
  my ($t,$c,$x,$y)=@_;
  my $s={};
  
  my $f=$c->Frame(-width=>70,-height=>220,-relief=>'raise',-borderwidth=>3)->place(-x=>$x,-y=>$y);
  
  my $cb4=$f->Checkbutton(-text=>'4',
                          -variable=>\$s->{b4},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>10);
  my $cb3=$f->Checkbutton(-text=>'3',
                          -variable=>\$s->{b3},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>35);
  my $cb2=$f->Checkbutton(-text=>'2',
                          -variable=>\$s->{b2},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>60);
  my $cb1=$f->Checkbutton(-text=>'1',
                          -variable=>\$s->{b1},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>85);
  my $cb0=$f->Checkbutton(-text=>'f',
                          -variable=>\$s->{b0},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>110);
  my $cbc=$f->Checkbutton(-text=>'><',
                          -variable=>\$s->{bc},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>135);
  my $cbo=$f->Checkbutton(-text=>'<>',
                          -variable=>\$s->{bo},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>160);
  my $cbs=$f->Checkbutton(-text=>'St',
                          -variable=>\$s->{bs},
                          -onvalue=>1,
                          -offvalue=>0
                          )->place(-x=>10,-y=>185);

  $s->{cb4}=$cb4;
  $s->{cb3}=$cb3;
  $s->{cb2}=$cb2;
  $s->{cb1}=$cb1;
  $s->{cb0}=$cb0;
  $s->{cbc}=$cbc;
  $s->{cbo}=$cbo;
  $s->{cbs}=$cbs;

  return bless($s,$t);
  }

sub test
  {
  my $s=shift;
  my $r=0;
  
  if($s->{b0}==1) { $r|=0x01; }
  if($s->{b1}==1) { $r|=0x02; }
  if($s->{b2}==1) { $r|=0x04; }
  if($s->{b3}==1) { $r|=0x08; }
  if($s->{b4}==1) { $r|=0x10; }
  if($s->{bc}==1) { $r|=0x20; }
  if($s->{bo}==1) { $r|=0x40; }
  if($s->{bs}==1) { $r|=0x80; }
  
  return $r;  
  }

sub rls
  {
  my $s=shift;
  my $w=shift;  

  if($w==0) { $s->{b0}=0; }
  if($w==1) { $s->{b1}=0; }
  if($w==2) { $s->{b2}=0; }
  if($w==3) { $s->{b3}=0; }
  if($w==4) { $s->{b4}=0; }
  if($w==5) { $s->{bc}=0; }
  if($w==6) { $s->{bo}=0; }
  if($w==7) { $s->{bs}=0; }
  }

1;  