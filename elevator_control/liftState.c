/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Ez a modul: a lift mozgását vezérli, illetve mozgásának állapotait állítja.
*****************************************************************************/

#include "def.h"
#include "func.h"
#include <stdio.h>


static int curDest = -1;	//tárold az előző hívást, mert ha megszűnik az összes hívás, akkor is menj el az adott emeletre

	static int startingSteps = 20;
	static int startingLeft = 20;
	static int toFastSteps = 20;
	static int toFastLeft = 20;
	static int toSlowSteps = 20;
	static int toSlowLeft = 20;
	static int stoppingSteps = 20;
	static int stoppingLeft = 20;

//moveToDest-hez:
	static int slowNo = 15;			//induláskor hány lassú parancs legyen
	static int slowLeft = 15;		//ebből mennyi van még hátra
	static int stoppingUpNo = 3;	//felfelé megálláskor mennyivel kell túlmenni a felső szenzoron
	static int stoppingUp = 3;		//mennyi van még hátra
	static int stoppingDownNo = 15;		//lefelé megálláskor mennyit kell még menni az előző emelet alsó szenzorának elengedésétől
	static int stoppingDown = 15;		//mennyi van még hátra
//

int savedState;		//a vészgomb megnyomása előtti állapotot ide mentjük

extern int haveCallHere(void);

//kezdetben keresd meg a legközelebbi emeletet, és onnan indulj
//akármilyen kezdőpontról indulva működik
//igazzal tér vissza, ha a pozícionálás befejeződött
static int goToPos = 1;

extern void sendCmd( int command );
extern int getState(void);

int initPosition(void) {


		if( sensorBottom[0] || sensorBottom[1] || sensorBottom[2] || sensorBottom[3] || sensorBottom[4] ) {
			if( !sensorBottom[0] ) {	//ha vmelyik sz. jelez, menj amíg ki nem enged
										//ha nem a fsz.en vagy, akkr le
				while( sensorBottom[0] || sensorBottom[1] || sensorBottom[2] || sensorBottom[3] || sensorBottom[4] ) {
					sendCmd( DOWN_SLOW );
					staBits = getState();
					processSta();
				}
				goToPos = 14;
			} else {
				while( !sensorBottom[1] ) {
					sendCmd( UP_SLOW );
					staBits = getState();
					processSta();
				}
				goToPos = 17;
			}
		} else {
			while( !(sensorBottom[0] || sensorBottom[1] || sensorBottom[2] || sensorBottom[3] || sensorBottom[4]) ) {
				sendCmd( UP_SLOW );		//ha egyik alsó szenzor se jelez, akkor menj fel, amíg nem találsz egy emeletet
				staBits = getState();
				processSta();

			}
			goToPos = 17;
		}

		while( goToPos > 0 ) {	//majd menj le újra, h szintben legyél
			sendCmd( DOWN_SLOW );
			staBits = getState();
			processSta();
			goToPos--;
		}

		//a jelenlegi szint beállítása
		int j;
		for( j=0; j<5 ;j++ ){
			if(sensorBottom[j]) {
				level = j;
				break;
			}
		}

	return 0;

}

void moveToDest(void) {

	if( dest != -1 ) {		//frissítsd az úticélt, de ha nem lenne már hívás, akkor is menj el az utolsó úticélhoz
		curDest = dest;
	}

	if( state == STOP_FORCED ) {	//stop gomb lekezelése
		if( !forcedStop ) 
			state = savedState;
		else {
			nextCmd |= STOP;
			return;
		}
	} else {
		if( forcedStop && state != STOPPED && state != WAITING ) {		//stop gomb -> ne menj sehova
			savedState = state;
			state = STOP_FORCED;
			nextCmd |= STOP;
			return;
		}
	}

	switch( state ) {

		case STOPPED:
			//lépés WAITING áll.-ba
			//<> gomb benyomva vagy van hívás ezen az emeleten
			if( openDoor || haveCallHere() ) {
				state = WAITING;
			} else if( dest != -1 && !forcedStop ) { //különben, ha van hívás, és a stop gomb sincs benyomva
				state = STARTING;		//lépés STARTING áll.-ba
			}
			nextCmd |= STOP;
			break;
		case STARTING:
			if( cmdStarting( &nextCmd ) ) {
				if( level == curDest )
					state = MOVING_SLOW;		//ha csak a szomszédos szintre megyünk, nincs már idő felgyorsítani
												//teljesen, ezért csak a lassú sebességet érjük el
				else
					state = SLOWTOFAST;			//SLOWTOFAST áll-ba
			}
			break;
		case SLOWTOFAST:
			if( cmdToFast( &nextCmd ) )		//MOVING áll-ba
				state = MOVING;
			break;
		case MOVING_SLOW:
				if( cmdSlow( &nextCmd ) )
					state = STOPPING;
			break;
		case MOVING:
			if( dir == DIR_UP ) {
				nextCmd |= UP_FAST;
			} else {
				nextCmd |= DOWN_FAST;
			}
			if( level == curDest )		//FASTTOSLOW -ba
				state = FASTTOSLOW;
			break;
		case FASTTOSLOW:
			if( cmdToSlow( &nextCmd ) )
				state = STOPPING;		//STOPPING áll-ba
			break;
		case STOPPING:
			if( cmdStopping( &nextCmd ) )		//WAITING áll-ba
				state = WAITING;
			break;
		case WAITING:
			if( doorState == CLOSED && !openDoor && !haveCallHere() )
				state = STOPPED;
			nextCmd |= STOP;
			break;
	}
/*
	//debug:
	switch(state) {
		case STOPPED: printf("State: STOPPED\n"); break;
		case WAITING: printf("State: WAITING\n"); break;
		case STARTING: printf("State: STARTING\n"); break;
		case SLOWTOFAST: printf("State: SLOWTOFAST\n"); break;
		case MOVING: printf("State: MOVING\n"); break;
		case FASTTOSLOW: printf("State: FASTTOSLOW\n"); break;
		case STOPPING: printf("State: STOPPING\n"); break;
	}*/

}
