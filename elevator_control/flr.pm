package FLR;

sub new
  {
  my ($t,$c,$x,$y)=@_;

  my $s={};
  
  my $sl=$c->createLine($x,$y,$x+10,$y,$x+10,$y+50,$x,$y+50,-width=>3,-fill=>'gray');
  my $sr=$c->createLine($x+50,$y,$x+40,$y,$x+40,$y+50,$x+50,$y+50,-width=>3,-fill=>'gray');

  my $i=$c->createLine($x,$y,$x+8,$y,-fill=>'red');
  my $i=$c->createLine($x,$y+50,$x+8,$y+50,-fill=>'red');

  $s->{up}=0;
  $s->{dn}=0;
  
  my $bu=$c->Checkbutton( -bg=>'gray',
                          -activebackground=>'gray',
                          -variable=>\$s->{up},
                          -onvalue=>1,
                          -offvalue=>0)->place(-x=>$x+55,-y=>$y);

  my $bd=$c->Checkbutton( -bg=>'gray',
                          -activebackground=>'gray',
                          -variable=>\$s->{dn},
                          -onvalue=>1,
                          -offvalue=>0)->place(-x=>$x+55,-y=>$y+25);

  $s->{x}=$x;
  $s->{y}=$y;
  $s->{c}=$c;
  $s->{bu}=$bu;
  $s->{bd}=$bd;

  return bless($s,$t);
  }

sub test
  {
  my $s=shift;
  
  my $bx=shift;
  
  my $x=$s->{x};
  my $y=$s->{y};
  my $c=$s->{c};
 
  my $r=0;

  my @cd=$bx->coords();
  
  if($cd[1]<$y+2 && $cd[3]>$y+2)   { $r|=0x01; }
  if($cd[1]<$y+50 && $cd[3]>$y+50) { $r|=0x02; }

  if($s->{up}!=0) { $r|=0x04; }
  if($s->{dn}!=0) { $r|=0x08; }

  return $r;
  }

sub clrup
  {
  my $s=shift;
  $s->{up}=0;
  }

sub clrdn
  {
  my $s=shift;
  $s->{dn}=0;
  }

1;