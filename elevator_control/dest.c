/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Ez a modul:	állítja a lift mozgási irányát, a következő emelet számát ahol 
				megáll,	és a jelenlegi emelet számát, ahol tartózkodik.
*****************************************************************************/

#include "def.h"
#include "func.h"
#include <stdio.h>

void setLevel(void) {

	int incUp = 0;
	//fel ment-e egy emeletet:
	if( level < 4 && dir == DIR_UP ) {
		if( sensorBottom[level+1] ) {
			level++;
			incUp = 1;
		}
	}
	//ha nem ment fel, nézd meg, hogy nem ment-e le
	if( !incUp && level > 0 && dir == DIR_DOWN ) {
		if( sensorTop[level-1] )
			level--;
	}

	//debug:
	//printf("level: %i\n", level); 
}

static int prevDest = -1;	//az előző úticél
static int prevDir = DIR_UP;	//az előző mozgási irány
	//használat: csak álló helyzetben engedd a liftet irányt váltani
	//(ez akkor lehetne probléma, ha a hívás megszűnne valahol)

//újra beállítja az úticélt, melyik emelet
//a setLevel után kell meghívni
void setDest(void) {

	int cLevel = level;		//az épp vizsgált szint száma
	//ha megvan az úticél, breakelj ki
	do {
		//ha épp felfelé mentél
		if( dir == DIR_UP ) {
			//először vizsgáld, hogy itt, vagy fentebb felfelé menő van-e:
				//ha u azon az emeleten ahol vagy van hívás:
				if( callUp[level] || toGo[level] ) {		
					//már az emeleten áll
					if( state == STOPPED ) {
						dest = level;
						break;
					}
					//ha mozgásban van: ha már az emeleten van, csak akkor van ideje megállni, ha még csak gyorsít
					//és még az emelet előtt van
					if( state == STARTING && !sensorTop[level] ) {
						dest = level;
						break;
					}
					//vagy épp az adott emeleten készül megállni
					if( dest == level ) {
						break;
					}
					//különben már túlhaladtunk, vagy nem lehet hirtelen megállni, tehát vizsgáld tovább a hívásokat

				}
				cLevel = level+1;
				while( cLevel < 5 ) {
					if( callUp[cLevel] || toGo[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel++;
				}
				if( cLevel < 5 )	break;	//breakelt az előző while-ból
			//ha u ezen a szinten és fentebb nem volt felfelé hívás, akkor nézd meg van-e lefelé hívás fentebb:
				cLevel = level+1;
				while( cLevel < 5 ) {
					if( callDown[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel++;
				}
				if( cLevel < 5 )	break;	//breakelt az előző while-ból

			//fentebb egyáltalán nem volt hívás, irány: le, van-e hívás lent lefelé?
				dir = DIR_DOWN;
				cLevel = level;
				//u. ezen a szinten lefelé:
				if( callDown[level] || toGo[level] ) {
					//már az emeleten áll
					if( state == STOPPED ) {
						dest = level;
						break;
					}
					//ha mozgásban van: ha már az emeleten van, csak akkor van ideje megállni, ha még csak gyorsít
					//és még az emelet előtt van
					if( state == STARTING && !sensorBottom[level] ) {
						dest = level;
						break;
					}
					//vagy épp az adott emeleten készül megállni
					if( dest == level ) {
						break;
					}

				}
				cLevel = level-1;
				while( cLevel >= 0 ) {
					if( callDown[cLevel] || toGo[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel--;
				}
				if( cLevel >= 0 )	break;	//breakelt az előző while-ból
			//ha lent nem votl lefelé hívás, van-e hívás lent felfelé?
				cLevel = level-1;
				while( cLevel >= 0 ) {
					if( callUp[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel--;
				}
				if( cLevel >= 0 )	break;	//breakelt az előző while-ból
		}
		else {		//különben: épp lefelé mentél:
			//van-e hívás ezen a szinten lefelé, vagy lentebb lefelé?
				cLevel = level;
				//u. ezen a szinten lefelé:
				if( callDown[level] || toGo[level] ) {
					//már az emeleten áll
					if( state == STOPPED ) {
						dest = level;
						break;
					}
					//ha mozgásban van: ha már az emeleten van, csak akkor van ideje megállni, ha még csak gyorsít
					//és még az emelet előtt van
					if( state == STARTING && !sensorBottom[level] ) {
						dest = level;
						break;
					}
					//vagy épp az adott emeleten készül megállni
					if( dest == level ) {
						break;
					}

				}
				cLevel = level-1;
				while( cLevel >= 0 ) {
					if( callDown[cLevel] || toGo[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel--;
				}
				if( cLevel >= 0 )	break;	//breakelt az előző while-ból
			//ha nincs alattunk hívás lefelé, van-e alattunk hívás felfelé?
				cLevel = level-1;
				while( cLevel >= 0 ) {
					if( callUp[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel--;
				}
				if( cLevel >= 0 )	break;	//breakelt az előző while-ból
			//ha nincs alattunk hívás, indulj felfelé, van-e hívás fent felfelé?
				dir = DIR_UP;
				//ha u azon az emeleten ahol vagy van hívás:
				if( callUp[level] || toGo[level] ) {
					//már az emeleten áll
					if( state == STOPPED ) {
						dest = level;
						break;
					}
					//ha mozgásban van: ha már az emeleten van, csak akkor van ideje megállni, ha még csak gyorsít
					//és még az emelet előtt van
					if( state == STARTING && !sensorTop[level] ) {
						dest = level;
						break;
					}
					//vagy épp az adott emeleten készül megállni
					if( dest == level ) {
						break;
					}

				}
				cLevel = level+1;
				while( cLevel < 5 ) {
					if( callUp[cLevel] || toGo[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel++;
				}
				if( cLevel < 5 )	break;	//breakelt az előző while-ból
			//ha nincs felettünk hívás felfelé, van-e hívás fent lefelé
				cLevel = level+1;
				while( cLevel < 5 ) {
					if( callDown[cLevel] ) {
						dest = cLevel;
						break;
					}
					cLevel++;
				}
				if( cLevel < 5 )	break;	//breakelt az előző while-ból
		}

		//ha eddig nem breakeltél ki, akkor nincs egy hívás se
		dest = -1;
		if( dir == DIR_UP )
			dir = DIR_DOWN;
		else
			dir = DIR_UP;
		
	} while (0);


	//csak álló helyzetben engedd a liftet irányt váltani
	//(ez akkor lehetne probléma, ha a hívás megszűnne valahol)
	//és csak STOPPED áll.ban, v már ajtó csukódásnál
	if( dest != prevDest || dir != prevDir ) {
		 if( dir == prevDir ) {
			prevDest = dest;
			prevDir = dir;
		} else {
			if(state == STOPPED || doorState == CLOSING) {
				prevDest = dest;
				prevDir = dir;
			} else {
				dest = prevDest;
				dir = prevDir;
			}
		}
	}

	//debug
	//printf( "Dest: %i\n", dest );
	//printf( "dir: %i\n", dir );

}
