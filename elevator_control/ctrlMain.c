/****************************************************************************
* Feladat: lift vezérlés
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Modulok: ctrlMain.c process.c dest.c liftState.c doorState.c getCmd.c timer.c
* Header fileok: def.h func.h
* A futtatáshoz szükséges data.dat.
*
* Fordítás és futtatás:
* ./start1.sh
*****************************************************************************/

#include <stdio.h>
#include "def.h"
#include "func.h"

enum liftStates state = STOPPED;
enum directions dir = DIR_UP;
enum doorStates doorState = CLOSED;

int nextCmd = 0;		//a következő küldendő parancs
int staBits = 0;		//a státusz regiszter másolata

//Lift státusz:
	int callUp[5];		//ha van az adott emeleten felfelé hívás, akkor igaz
	int callDown[5];	//ha van az adott emeleten lefelé hívás, akkor igaz
	int toGo[5];		//ha az adott emelet az úticélok között van (belső gombok), akkor igaz
	int sensorTop[5];		//az adott emelet felső szenzora jelez-e
	int sensorBottom[5];	//az adott emelet alsó szenzora jelez-e
	int forcedStop;		//be van-e nyomva a stop gomb
	int openDoor;		//<> gomb be van-e nyomva
	int closeDoor;		//>< gomb be van-e nyomva
	int doorClosed;		//ajtó zárva szenzor
	int doorOpen;		//ajtó nyitva szenzor
//

int dest = -1;		//választott következő állomás (0-5 szint)
int level = 3;		//a jelenlegi szint (0-5)

static FILE*  staFp;
static FILE*  cmdFp;

void sendCmd( int command );
int getState(void);

extern FILE* dataFp;

int main(void) {

	int running = 1;

	printf("Lift control is running.\n");

	staFp = fopen("sta","r+");
	cmdFp = fopen("cmd","r+");

	if(staFp == NULL) {
		printf("Couldn't open FIFO called sta. Create the FIFO by \"mkfifo sta\" before running. Exiting...\n");
		running = 0;
	}
	if(cmdFp == NULL) {
		printf("Couldn't open FIFO called cmd. Create the FIFO by \"mkfifo cmd\" before running. Exiting...\n");
		running = 0;
	}
	if( openDataFile() == -1 ) {
		printf("Couldn't open data.dat. Exiting...\n");
		running = 0;
	}

	if(running) {
		sendCmd( STOP );
		staBits = getState();
		processSta();

		initPosition();
	}

	while(running) {

		nextCmd = 0;

		processSta();
		setLevel();
		setDest();
		
		moveToDest();

		controlDoor();

		sendCmd( nextCmd );

		staBits = getState();

	}

	if(staFp != NULL)
		fclose(staFp);
	if(cmdFp != NULL)
		fclose(cmdFp);
	if(dataFp != NULL)
		fclose(dataFp);

	return 0;
}

void sendCmd( int command )  {
	fprintf(cmdFp,"_%6.6X\n", command);
	fflush(cmdFp);
}

//blocking
int getState(void) {
	char ch;
	int STAin, r;
	//olvasd ki a státuszt: _4bytehex
	r = fscanf(staFp,"%c%X",&ch, &STAin);
	if(r!=2)
		printf("Error reading sta. Number of items read from sta: %i\n", r);
	//olvasd ki utána a benntmaradt entert:
	fscanf(staFp,"%c",&ch);
	return STAin;
}

