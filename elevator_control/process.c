/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Ez a modul:	feldolgozza a státusz biteket és könyebben kezelhető formába
				alakítja át.
*****************************************************************************/

#include "def.h"
#include "func.h"
#include <stdio.h>

void processSta(void) {
	
	callUp[0] = staBits & F0_UP_SET;
	callUp[1] = staBits & F1_UP_SET;
	callUp[2] = staBits & F2_UP_SET;
	callUp[3] = staBits & F3_UP_SET;

	callDown[1] = staBits & F1_DOWN_SET;
	callDown[2] = staBits & F2_DOWN_SET;
	callDown[3] = staBits & F3_DOWN_SET;
	callDown[4] = staBits & F4_DOWN_SET;

	toGo[0] = staBits & B_0_SET;
	toGo[1] = staBits & B_1_SET;
	toGo[2] = staBits & B_2_SET;
	toGo[3] = staBits & B_3_SET;
	toGo[4] = staBits & B_4_SET;

	sensorTop[0] = staBits & F0_TOP;
	sensorTop[1] = staBits & F1_TOP;
	sensorTop[2] = staBits & F2_TOP;
	sensorTop[3] = staBits & F3_TOP;
	sensorTop[4] = staBits & F4_TOP;

	sensorBottom[0] = staBits & F0_BOTTOM;
	sensorBottom[1] = staBits & F1_BOTTOM;
	sensorBottom[2] = staBits & F2_BOTTOM;
	sensorBottom[3] = staBits & F3_BOTTOM;
	sensorBottom[4] = staBits & F4_BOTTOM;

	forcedStop = staBits & STOP_SET;

	openDoor = staBits & OPEN_DOOR_SET;
	closeDoor = staBits & CLOSE_DOOR_SET;

	doorClosed = staBits & DOOR_CLOSED;
	doorOpen = staBits & DOOR_OPEN;

	//debug:
//	printf("-----------\n");
	//printf("callUp: %i %i %i %i %i\n", callUp[0], callUp[1], callUp[2], callUp[3], callUp[4] );
	//printf("callDown: %i %i %i %i %i\n", callDown[0], callDown[1], callDown[2], callDown[3], callDown[4] );
	//printf("toGo: %i %i %i %i %i\n", toGo[0], toGo[1], toGo[2], toGo[3], toGo[4] );
//	printf("sensorTop   : %i %i %i %i %i\n", !!sensorTop[0], !!sensorTop[1], !!sensorTop[2], !!sensorTop[3], !!sensorTop[4] );
//	printf("sensorBottom: %i %i %i %i %i\n", !!sensorBottom[0], !!sensorBottom[1], !!sensorBottom[2], !!sensorBottom[3], !!sensorBottom[4] );
	/*printf("forcedStop: %i\n", forcedStop);
	printf("openDoor: %i\n", openDoor);
	printf("closeDoor: %i\n", closeDoor);
	printf("doorClosed: %i\n", doorClosed);
	printf("doorOpen: %i\n", doorOpen);
	*/
}

