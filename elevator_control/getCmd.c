/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Ez a modul:	a lift gyorsítása, illetve lassítása során a finomabb mozgást
*				valósítja meg. A liftState.c használja a függvényeit.
*****************************************************************************/

#include "def.h"
#include "func.h"
#include <unistd.h>
#include <stdio.h>

static u32 toSleep = 0;

static int periodCnt = 0;

static int toFastSteps = 45;
static int toFastCnt= 0;

static int addFastStepsDown = 12;
static int addFastLeftDown = 12;
static int addFastStepsUp = 16;
static int addFastLeftUp = 16;
static int toSlowSteps = 45;
static int toSlowCnt = 45;

static int slowStepsUp = 60;
static int slowLeftUp = 60;
static int slowStepsDown = 60;
static int slowLeftDown = 60;

//korábban az eltelt időt mérve számítottam ki, mekkora késleltetés kell
//ahhoz, hogy a lift lassan, fokozatosan indlujon/álljon meg
//mivel azonban ezt befolyásolhatja a két program futási sebessége, illetve
// a várakozás pontatlansága, ezért biztosabbnak láttam, ha a várakozási időket egy fileból olvasom vissza
//így a lift biztosan mindig ugyan annyi parancsot kap, tehát biztosan nem lesz elcsúszás a szintben
FILE* dataFp;

int openDataFile(void) {
	dataFp = fopen( "data.dat", "r" );
	if (dataFp == NULL)		return -1;
	return 0;
}

//visszatérési érték: igaz, ha vége a gyorsításnak/lassításnak, vagyis a következő állapot jön
//int *cmd: a következő parancs (csak lift mozgatás)
int cmdStarting( int *cmd ){
	if( cmd == 0 )	return -1;
	int ret = -2;
	
	if( dataFp == NULL ) { printf("Error: data file is missing!\n"); return -1; }
	int in = 0;
	fscanf( dataFp, "%i", &in );

	if( in > 0 ) {
		toSleep = (u32)in;
		usleep( toSleep );
		ret = 0;
	} else {
		ret = 1;
	}
	if( dir == DIR_UP )
		*cmd |= UP_SLOW;
	else
		*cmd |= DOWN_SLOW;

	return ret;
}

int cmdStopping( int *cmd ) {
	if( cmd == 0 )	return -1;
	int ret = -2;

	if( dataFp == NULL ) { printf("Error: data file is missing!\n"); return -1; }
	if( fscanf( dataFp, "%i", &toSleep ) != EOF ) {
		usleep( toSleep );
		if( dir == DIR_UP )
			*cmd |= UP_SLOW;
		else
			*cmd |= DOWN_SLOW;
		ret = 0;
	} else {
		ret = 1;
		*cmd |= STOP;
		fseek( dataFp, 0, SEEK_SET );
	}

	return ret;
}
int cmdToFast( int *cmd ){

	if( cmd == 0 )	return -1;
	int fast;
	int slow;
	if( dir == DIR_UP ) {
		fast = UP_FAST;
		slow = UP_SLOW;
	}
	else {
		fast = DOWN_FAST;
		slow = DOWN_SLOW;
	}
	if(periodCnt) {
		nextCmd |= fast;
	} else {
		if( toFastCnt < toFastSteps/3 ) {
			periodCnt = 2;
			usleep(20000);	//20150 túl lassú volt
			*cmd |= slow;
		} else if ( toFastCnt < toFastSteps*2/3 )  { periodCnt = 2; *cmd |=  slow; }
		else if ( toFastCnt < toFastSteps ) { periodCnt = 1; usleep(4000); *cmd |= fast; }
		else { *cmd |= fast; periodCnt = 0; toFastCnt = 0; return 1; }	//köv áll.-ba
	}
	periodCnt--;
	toFastCnt++;
	
	return 0;
}
int cmdToSlow( int *cmd ){

	if( cmd == 0 )	return -1;
	int fast;
	int slow;
	if( dir == DIR_UP ) {
		fast = UP_FAST;
		slow = UP_SLOW;
		if( addFastLeftUp ) {
			*cmd |= fast;
			addFastLeftUp--;
			return 0;
		}
	}
	else {
		fast = DOWN_FAST;
		slow = DOWN_SLOW;
		if( addFastLeftDown ) {
			*cmd |= fast;
			addFastLeftDown--;
			return 0;
		}
	}
	if(periodCnt) {
		nextCmd |= fast;
	} else {
		if ( toSlowCnt > toSlowSteps*2/3 ) { periodCnt = 1; usleep(4000); *cmd |= fast; }
 		else if ( toSlowCnt > toSlowSteps/3  )  { periodCnt = 2; *cmd |=  slow; }
		else if ( toSlowCnt > 0 ) {
			periodCnt = 2;
			usleep(20000);	//20150 túl lassú volt
			*cmd |= slow;
		} else {	//köv. áll.-ba
			*cmd |= slow;
			periodCnt = 0;
			toSlowCnt = toSlowSteps;
			addFastLeftDown = addFastStepsDown;
			addFastLeftUp = addFastStepsUp;
			return 1;
		}
	}
	periodCnt--;
	toSlowCnt--;

	return 0;
}

int cmdSlow( int *cmd ) {

	if( cmd == 0 )	return -1;
	int slow;
	if( dir == DIR_UP ) {
		slow = UP_SLOW;
		if( slowLeftUp > 0 ) {
			slowLeftUp--;
			*cmd |= slow;
		} else {
			slowLeftUp = slowStepsUp;
			return 1;
		}
	}
	else { 
		slow = DOWN_SLOW;
		if( slowLeftDown > 0 ) {
			slowLeftDown--;
			*cmd |= slow;
		} else {
			slowLeftDown = slowStepsDown;
			return 1;
		}
	}

	return 0;
}
