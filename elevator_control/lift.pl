#!/usr/bin/perl

use Tk;
use Fcntl;
use IO::Handle;
use flr;
use hit;
use box;

my $dr=1;
my $x=50;

open(STA,"+<./sta");
open(CMD,"+<./cmd");

$flags=fcntl(CMD,F_GETFL,0);
$flags=fcntl(CMD,F_SETFL,$flags|O_NONBLOCK);
$flags=fcntl(STA,F_GETFL,0);
$flags=fcntl(STA,F_SETFL,$flags|O_NONBLOCK);

$mw=MainWindow->new();

$mw->geometry("310x410+200+0");

$c=$mw->Canvas(	-width	=>300,
                -height	=>400,
                -relief	=>'sunken',
                -bg	=>'black')->place(-x=>5,-y=>5);

$hitup=HITUP->new($c,$x,45);
$f4=FLR->new($c,$x,50);
$f3=FLR->new($c,$x,105);
$f2=FLR->new($c,$x,160);
$f1=FLR->new($c,$x,215);
$f0=FLR->new($c,$x,270);
$hitdn=HITDN->new($c,$x,325);

$bx=BOX->new($c,$x+13,100);

$tb=TBL->new($c,$x+100,50);

$mw->repeat(20,\&doit);

MainLoop();

sub doit
  {
  $f0->clrdn();
  $f4->clrup();
  
  my $sf0=$f0->test($bx);
  my $sf1=$f1->test($bx);
  my $sf2=$f2->test($bx);
  my $sf3=$f3->test($bx);
  my $sf4=$f4->test($bx);
  my $hus=$hitup->test($bx);
  my $hds=$hitdn->test($bx);
  my $bxs=$bx->test();
  my $tbs=$tb->test();  

  my $r=sprintf("_%2.2X%X%X%X%X%X%X\n",$tbs,$hus+$hds+$bxs,$sf4,$sf3,$sf2,$sf1,$sf0);

#  print $r;

  my $cmd=<CMD>;
  if($cmd eq "") { return; }
  print STA $r; STA->autoflush(1);
  chomp($cmd);
  
  my $w;
  my $d;
  my $sp;
  
  my @cb=split(//,$cmd);
  my $c0=hex($cb[6]);
  my $c1=hex($cb[5]);
  my $c2=hex($cb[4]);
  my $c3=hex($cb[3]);
  my $c4=hex($cb[2]);
  my $c5=hex($cb[1]);
  
  $w=$c0 & 7;
  if($w == 0) 		{ #$bx->move(0,0); 
                          goto DOOR; }
  if(($w & 3) == 3)	{ #$bx->move(0,0); 
                          goto DOOR; }
  if($w & 4)		{ $sp = 1; }
  else			{ $sp = 2; }

  $w=$w & 3;

  if($hus!=0 && $w==1)	{ $bx->move(0,0); goto DOOR; }
  if($hds!=0 && $w==2)	{ $bx->move(0,0); goto DOOR; }

  if($w==1)		{ $bx->move(1,$sp);  }
  if($w==2)		{ $bx->move(-1,$sp); }

DOOR:  

  $w=$c0;
  if($w & 8)		{ $bx->open();  }
  else			{ $bx->close(); }

  if($c1 == 0)		{ goto F23; 	}
  if($c1 & 1)		{ $f0->clrdn();	} 
  if($c1 & 2)		{ $f0->clrup();	}
  if($c1 & 4)		{ $f1->clrdn();	} 
  if($c1 & 8)		{ $f1->clrup();	}
  
F23:

  if($c2 == 0)		{ goto F4; 	}
  if($c2 & 1)		{ $f2->clrdn();	} 
  if($c2 & 2)		{ $f2->clrup();	}
  if($c2 & 4)		{ $f3->clrdn();	} 
  if($c2 & 8)		{ $f3->clrup();	}
  
F4:

  if($c3 & 1)		{ $f4->clrdn();	} 
  if($c3 & 2)		{ $f4->clrup();	}

  if($c4 & 1)		{ $tb->rls(0);	}
  if($c4 & 2)		{ $tb->rls(1);	}
  if($c4 & 4)		{ $tb->rls(2);	}
  if($c4 & 8)		{ $tb->rls(3);	}
  
  if($c5 & 1)		{ $tb->rls(4);	}
  if($c5 & 2)		{ $tb->rls(5);	}
  if($c5 & 4)		{ $tb->rls(6);	}
  if($c5 & 8)		{ $tb->rls(7);	}
  }

# 0. byte
# 2.1.0. bit 000 allj 001 fel lassan 010 le lassan 101 fel gyorsan 110 le gyorsan
# 3.     bit 0 ajto zar 1 ajto nyit
# 4.     f0 le torol ilyen nem lehet
# 5.     f0 fel torol   
# 6.     f1 le torol
# 7.     f1 fel torol

# 1. byte
# 0.     f2 le torol
# 1.     f2 fel torol
# 2.     f3 le torol
# 3.     f3 fel torol
# 4.     f4 le torol
# 5.     f4 fel torol ilyen nem lehet

# 2. byte
# 0.     0. gomb torol
# 1.     1. gomb torol
# 2. 	 2. gomb torol
# 3.     3. gomb torol
# 4.     4. gomb torol
# 5.     >< gomb torol
# 6.     <> gomb torol
# 7.     stop torol

