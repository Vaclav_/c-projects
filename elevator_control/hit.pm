package HITUP;

sub new
  {
  my ($t,$c,$x,$y)=@_;
  my $s={};
  
  $l=$c->createLine($x,$y,$x+50,$y,-fill=>'red',-width=>3);
  my $s->{x}=$x;
  my $s->{y}=$y;
  my $c->{c}=$c;  

  return bless($s,$t);  
  }
  
sub test
  {
  my $s=shift;
  my $bx=shift;
  
  my $x=$s->{x};
  my $y=$s->{y};
  
  my @cd=$bx->coords();
  
  if($cd[1]<=$y) { return 4; }
  else		 { return 0; } 
  }

package HITDN;

sub new
  {
  my ($t,$c,$x,$y)=@_;
  my $s={};
  
  $l=$c->createLine($x,$y,$x+50,$y,-fill=>'red',-width=>3);
  my $s->{x}=$x;
  my $s->{y}=$y;
  my $c->{c}=$c;  

  return bless($s,$t);  
  }
  
sub test
  {
  my $s=shift;
  my $bx=shift;
  
  my $x=$s->{x};
  my $y=$s->{y};
  
  my @cd=$bx->coords();
  
  if($cd[3]>=$y) { return 8; }
  else		 { return 0; } 
  }

1;