/****************************************************************************
* A lift vezérlés feladat része. Lásd ctrlMain.c
* Készítette: Túri Patrik
* Utolsó módosítás: 2012 10. 9.
*
* Ez a modul: az ajtó mozgatása és az ajtó állapotának állítása
*****************************************************************************/

#include "def.h"
#include "func.h"
#include <stdio.h>

#define WAIT_TIME 5*1000*1000
//us

static void clearCallsHere(void);

static int cInsideBefore[5];	//calls inside before stopping, milyen hívások voltak mielőtt megálltunk
static void copyCalls(void);	//csak a belül lévőket
static int isNewCall(void);		//új hívás van-e belül
int haveCallHere(void);	//van-e hívás ezen az emeleten
static void clearAnyCallsHere(void);
static int noOtherCalls(void);
static int callOtherDir(void);

static int rTStart = 3;
static int reactionTime = 3;

void controlDoor(void) {

	//nincs be és kiszállás, ha csak nem várakozik, v. áll
	if( state != WAITING && state != STOPPED ) {
		if( openDoor )
			nextCmd |= OPEN_DOOR_CL;
		if( closeDoor )
			nextCmd |= CLOSE_DOOR_CL;
		return;
	}

	//cont. here, in this function modify doorstate only!
	switch( doorState ) {
		case CLOSED:
			nextCmd |= CLOSE_DOOR_CL;
			//ha van hívás:
			if( openDoor || haveCallHere() || ( noOtherCalls() && callOtherDir() )) {
				doorState = OPENING;
				nextCmd |= OPEN_DOOR_CL;
			} else
				break;
			//szándékosan nincs break
		case OPENING:
			//printf("OPENING\n");
			nextCmd |= OPEN_DOOR;
			nextCmd |= OPEN_DOOR_CL;
			if( closeDoor ) {
				nextCmd |= CLOSE_DOOR_CL;
				doorState = CLOSING;
			} else if( doorOpen ) {
				doorState = OPEN;
				timerStart();
				copyCalls();
			}
			break;
		case OPEN:
			//printf("OPEN\n");
			if( openDoor || haveCallHere() || ( noOtherCalls() && callOtherDir() ) ) {
				timerStart();
				nextCmd |= OPEN_DOOR_CL;
			}
			if( closeDoor || isNewCall() || timerCheck() >= WAIT_TIME ) {		//ajtó bezár, vagy van már belül új úticél
				doorState = CLOSING;
				nextCmd |= CLOSE_DOOR_CL;
				reactionTime = rTStart;
			} else			//továbbra is nyitva az ajtó
				nextCmd |= OPEN_DOOR;
			break;
		case CLOSING:
			//printf("CLOSING\n");
			if(reactionTime)
				reactionTime--;
			nextCmd |= CLOSE_DOOR;
			nextCmd |= CLOSE_DOOR_CL;
			//még van beszálló
			if( ( (openDoor || haveCallHere()) || ( noOtherCalls() && callOtherDir() ) ) && !reactionTime ) {
				doorState = OPENING;
				nextCmd |= OPEN_DOOR_CL;
			} else if( doorClosed )
				doorState = CLOSED;
			break;
		default:
			break;
	}

	clearCallsHere();
	if( ( noOtherCalls() && callOtherDir() ) )
		clearAnyCallsHere();

}

int noOtherCalls(void) {
	int i;
	for(i=0; i<5; i++) {
		if( i == level )
			continue;
		if( callUp[i] || callDown[i] || toGo[i] )
			return 0;
	}
	return 1;
}

int callOtherDir(void) {
	if( dir == DIR_UP )
		return( callDown[level] );
	else
		return( callUp[level] );
}

//töröld az adott emeleten a hívásokat, ahol vagyunk
//csak a mozgási irányunknak megfelelő fel v le hívást töröld, a másikért majd visszajövünk
void clearCallsHere(void) {
	switch (level) {
		case 0:
			nextCmd |= F0_UP_CL | B_0_CL;
			break;
		case 1:
			if( dir == DIR_UP )
				nextCmd |= F1_UP_CL | B_1_CL;
			else
				nextCmd |= F1_DOWN_CL | B_1_CL;
			break;
		case 2:
			if( dir == DIR_UP )
				nextCmd |= F2_UP_CL | B_2_CL;
			else
				nextCmd |= F2_DOWN_CL | B_2_CL;
			break;
		case 3:
			if( dir == DIR_UP )
				nextCmd |= F3_UP_CL | B_3_CL;
			else
				nextCmd |= F3_DOWN_CL | B_3_CL;
			break;
		case 4:
			nextCmd |= F4_DOWN_CL | B_4_CL;
			break;
		default:
			break;
	}
}

void clearAnyCallsHere(void) {
	switch (level) {
		case 0:
			nextCmd |= F0_UP_CL | B_0_CL;
			break;
		case 1:
			nextCmd |= F1_UP_CL | F1_DOWN_CL | B_1_CL;
			break;
		case 2:
			nextCmd |= F2_UP_CL | F2_DOWN_CL | B_2_CL;
			break;
		case 3:
			nextCmd |= F3_UP_CL | F3_DOWN_CL | B_3_CL;
			break;
		case 4:
			nextCmd |= F4_DOWN_CL | B_4_CL;
			break;
		default:
			break;
	}
}

//csak a mi irányunkba menő számít (fel/le)
int haveCallHere(void) {
	if( level>0 && level < 4 ) {
		if( dir == DIR_UP )
			return( callUp[level] || toGo[level] );
		else
			return( callDown[level] || toGo[level] );
	} else
		return( callUp[level] || callDown[level] || toGo[level] );
}

void copyCalls(void) {
	int i;
	for( i=0; i<5; i++) {
		cInsideBefore[i] = toGo[i];
	}
}

int isNewCall(void) {
	int i,ret=0;
	for( i=0; i<5; i++) {
		if( (cInsideBefore[i] != toGo[i]) && (i != level) && toGo[i]) {
			copyCalls();
			ret = 1;
			break;
		}	//esle: copy all
	}
	return ret;
}
